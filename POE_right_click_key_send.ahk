#IfWinActive, Path of Exile
#SingleInstance Force
#NoEnv

~RButton::
MouseGetPos, x,y
WinGetPos, , , Width, Height
div_p := % (Floor(Height/20))
x_Max := % (Width-(4*div_p+16))
y_Max := % (Height-(div_p+8))

if (x > x_Max-div_p and x < x_Max  and y < Height and y > y_Max)

    Send {T}

if (x > x_Max-(div_p*2) and x < x_Max-div_p  and y < Height and y > y_Max)
    Send {R}

if (x > x_Max-(div_p*3) and x < x_Max-(div_p*2)  and y < Height and y > y_Max)
    Send {E}

if (x > x_Max-(div_p*4) and x < x_Max-(div_p*3)  and y < Height and y > y_Max)
    Send {W}

if (x > x_Max-(div_p*5) and x < x_Max-(div_p*4)  and y < Height and y > y_Max)
    Send {Q}

return